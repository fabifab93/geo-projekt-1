package charts;

import data.Data;
import data.HeatDemandBuilding2_2012;
import data.HeatDemandBuilding2_2013;
import data.HeatDemandBuilding2_2014;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class a_PieChartHeatDemand2012_2014 extends Application {

	/**
	 * Creates a data set for pie charts.
	 * @param data
	 * @return CategoryDataset
	 */
	private ObservableList<PieChart.Data> createPieDataSet(Data data) {
		ObservableList<PieChart.Data> list = FXCollections.observableArrayList();
		
		for(int i = 0; i < data.getObservationSpace().length; i++) {
			list.add(new PieChart.Data(
					data.getObservationSpace()[i],
					data.getCharacteristicValue()[i].doubleValue()
					));
		}
		
		return list;
	}
	
	
	@Override
	public void start(Stage stage) throws Exception {
		// Data to be displayed
		Data data2012 = new HeatDemandBuilding2_2012();
		Data data2013 = new HeatDemandBuilding2_2013();
		Data data2014 = new HeatDemandBuilding2_2014();
		
		// Creation of data sets
		ObservableList<PieChart.Data> pieChartData2012 = createPieDataSet(data2012);
		ObservableList<PieChart.Data> pieChartData2013 = createPieDataSet(data2013);
		ObservableList<PieChart.Data> pieChartData2014 = createPieDataSet(data2014);
		
		// Creation of pie charts
		final PieChart pieChart2012 = new PieChart(pieChartData2012);
		final PieChart pieChart2013 = new PieChart(pieChartData2013);
		final PieChart pieChart2014 = new PieChart(pieChartData2014);
		
		// Labeling of pie charts
		pieChart2012.setTitle("Pie Chart 2012");
		pieChart2013.setTitle("Pie Chart 2013");
		pieChart2014.setTitle("Pie Chart 2014");
		
		// Rendering charts grouped in horizontal box
		HBox hbox = new HBox(pieChart2012, pieChart2013, pieChart2014);
		Scene scene = new Scene(hbox, 800, 600);
		
		stage.setTitle(data2012.getTopic());
		stage.setScene(scene);
		stage.show();
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
}
