package charts;

import data.Data;
import data.HeatDemandBuilding2_2012;
import data.HeatDemandBuilding2_2013;
import data.HeatDemandBuilding2_2014;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.BubbleChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

public class e_BubbleChartHeatDemand2012_2014 extends Application {

	/**
	 * Creates a data set for area charts.
	 * @param data
	 * @return CategoryDataset
	 */
	private XYChart.Series<Number, Number> createBubbleDataSet(Data data) {
		XYChart.Series<Number, Number> series = new XYChart.Series<Number, Number>();
		
		for(int i = 0; i < data.getObservationSpace().length; i++) {
			series.getData().add(new XYChart.Data<Number, Number>(
					i+1,
					data.getCharacteristicValue()[i].doubleValue() + 1,
					data.getCharacteristicValue()[i].doubleValue() / 100
					));
		}
		
		return series;
	}
	
	
	@Override
	public void start(Stage stage) throws Exception {
		// Data to be displayed
		Data data2012 = new HeatDemandBuilding2_2012();
		Data data2013 = new HeatDemandBuilding2_2013();
		Data data2014 = new HeatDemandBuilding2_2014();
		
		// Creation of data sets
		XYChart.Series<Number, Number> series2012 = createBubbleDataSet(data2012);
		XYChart.Series<Number, Number> series2013 = createBubbleDataSet(data2013);
		XYChart.Series<Number, Number> series2014 = createBubbleDataSet(data2014);
		
		series2012.setName("2012");
		series2013.setName("2013");
		series2014.setName("2014");
		
		// Creation of bubble chart with axes
		final NumberAxis x = new NumberAxis(0, 12, 1);
		final NumberAxis y = new NumberAxis(0, 200, 25);
		
		x.setLabel(data2012.getObservationSpaceName());
		y.setLabel(data2012.getChararacteristicValueName());
		
		final BubbleChart<Number, Number> bubbleChart = new BubbleChart<Number, Number>(x, y);
		bubbleChart.getData().add(series2012);
		bubbleChart.getData().add(series2013);
		bubbleChart.getData().add(series2014);

		bubbleChart.setTitle("Bubble Chart 2012-2014");
		
		// Rendering the area chart
		Scene scene = new Scene(bubbleChart, 800, 600);
		
		stage.setTitle(data2012.getTopic());
		stage.setScene(scene);
		stage.show();
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}

}
