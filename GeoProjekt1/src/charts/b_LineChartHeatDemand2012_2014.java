package charts;

import data.Data;
import data.HeatDemandBuilding2_2012;
import data.HeatDemandBuilding2_2013;
import data.HeatDemandBuilding2_2014;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

public class b_LineChartHeatDemand2012_2014 extends Application {

	/**
	 * Creates a data set for line charts.
	 * @param data
	 * @return CategoryDataset
	 */
	private XYChart.Series<String, Number> createLineDataSet(Data data) {
		XYChart.Series<String, Number> series = new XYChart.Series<String, Number>();
		
		for(int i = 0; i < data.getObservationSpace().length; i++) {
			series.getData().add(new XYChart.Data<String, Number>(
					data.getObservationSpace()[i],
					data.getCharacteristicValue()[i].doubleValue()
					));
		}
		
		return series;
	}
	
	
	@Override
	public void start(Stage stage) throws Exception {
		// Data to be displayed
		Data data2012 = new HeatDemandBuilding2_2012();
		Data data2013 = new HeatDemandBuilding2_2013();
		Data data2014 = new HeatDemandBuilding2_2014();
		
		// Creation of data sets
		XYChart.Series<String, Number> series2012 = createLineDataSet(data2012);
		XYChart.Series<String, Number> series2013 = createLineDataSet(data2013);
		XYChart.Series<String, Number> series2014 = createLineDataSet(data2014);
		
		series2012.setName("2012");
		series2013.setName("2013");
		series2014.setName("2014");
		
		// Creation of line chart with axes
		final CategoryAxis x = new CategoryAxis();
		final NumberAxis y = new NumberAxis();
		
		x.setLabel(data2012.getObservationSpaceName());
		y.setLabel(data2012.getChararacteristicValueName());
		
		final LineChart<String, Number> lineChart = new LineChart<String, Number>(x, y);
		lineChart.getData().add(series2012);
		lineChart.getData().add(series2013);
		lineChart.getData().add(series2014);
		
		lineChart.setTitle("Line Chart 2012-2014");
		
		// Rendering the line chart
		Scene scene = new Scene(lineChart, 800, 600);
		
		stage.setTitle(data2012.getTopic());
		stage.setScene(scene);
		stage.show();
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}

}
