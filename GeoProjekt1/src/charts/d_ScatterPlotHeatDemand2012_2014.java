package charts;

import data.Data;
import data.HeatDemandBuilding2_2012;
import data.HeatDemandBuilding2_2013;
import data.HeatDemandBuilding2_2014;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

public class d_ScatterPlotHeatDemand2012_2014 extends Application {

	/**
	 * Creates a data set for area charts.
	 * @param data
	 * @return CategoryDataset
	 */
	private XYChart.Series<String, Number> createScatterDataSet(Data data) {
		XYChart.Series<String, Number> series = new XYChart.Series<String, Number>();
		
		for(int i = 0; i < data.getObservationSpace().length; i++) {
			series.getData().add(new XYChart.Data<String, Number>(
					data.getObservationSpace()[i],
					data.getCharacteristicValue()[i].doubleValue()
					));
		}
		
		return series;
	}
	
	
	@Override
	public void start(Stage stage) throws Exception {
		// Data to be displayed
		Data data2012 = new HeatDemandBuilding2_2012();
		Data data2013 = new HeatDemandBuilding2_2013();
		Data data2014 = new HeatDemandBuilding2_2014();
		
		// Creation of data sets
		XYChart.Series<String, Number> series2012 = createScatterDataSet(data2012);
		XYChart.Series<String, Number> series2013 = createScatterDataSet(data2013);
		XYChart.Series<String, Number> series2014 = createScatterDataSet(data2014);
		
		series2012.setName("2012");
		series2013.setName("2013");
		series2014.setName("2014");
		
		// Creation of area chart with axes
		final CategoryAxis x = new CategoryAxis();
		final NumberAxis y = new NumberAxis();
		
		x.setLabel(data2012.getObservationSpaceName());
		y.setLabel(data2012.getChararacteristicValueName());
		
		final ScatterChart<String, Number> scatterChart = new ScatterChart<String, Number>(x, y);
		scatterChart.getData().add(series2012);
		scatterChart.getData().add(series2013);
		scatterChart.getData().add(series2014);
		
		scatterChart.setTitle("Scatter Chart 2012-2014");
		
		// Rendering the area chart
		Scene scene = new Scene(scatterChart, 800, 600);
		
		stage.setTitle(data2012.getTopic());
		stage.setScene(scene);
		stage.show();
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}

}
