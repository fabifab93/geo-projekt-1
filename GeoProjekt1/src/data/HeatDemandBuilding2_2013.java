package data;

public class HeatDemandBuilding2_2013 implements Data {
	
	String[] observationSpace = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	Number[] characteristicValues = {144.44, 133.07, 121.78, 63.86, 35.53, 18.57, 0, 0, 16.56, 48.53, 91.88, 115.2};
	
	
	public String[] getObservationSpace() {
		return observationSpace;
	}


	public String getObservationSpaceName() {
		return "Month";
	}


	public Number[] getCharacteristicValue() {
		return characteristicValues;
	}


	public String getChararacteristicValueName() {
		return "Heat Demand [MWh]";
	}


	public String getTopic() {
		return "Heat Demand HFT Stuttgart Building 2 (per month)";
	}
	
}
