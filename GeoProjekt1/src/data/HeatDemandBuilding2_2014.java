package data;

public class HeatDemandBuilding2_2014 implements Data {
	
	String[] observationSpace = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	Number[] characteristicValues = {113.77, 96.17, 77.74, 38.27, 29.79, 11.60, 0, 0, 16.85, 36.14, 67.55, 138.00};
	
	
	public String[] getObservationSpace() {
		return observationSpace;
	}


	public String getObservationSpaceName() {
		return "Month";
	}


	public Number[] getCharacteristicValue() {
		return characteristicValues;
	}


	public String getChararacteristicValueName() {
		return "Heat Demand [MWh]";
	}


	public String getTopic() {
		return "Heat Demand HFT Stuttgart Building 2 (per month)";
	}
	
}
