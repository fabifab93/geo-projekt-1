package data;

public interface Data {

	// Observation space
	public String[] getObservationSpace();
	public String getObservationSpaceName();
	
	// Characteristic values
	public Number[] getCharacteristicValue();
	public String getChararacteristicValueName();
	
	// Topic
	public String getTopic();
	
}
