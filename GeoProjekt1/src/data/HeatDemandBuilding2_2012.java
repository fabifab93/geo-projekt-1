package data;

public class HeatDemandBuilding2_2012 implements Data {
	
	String[] observationSpace = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	Number[] characteristicValues = {128.52, 187.54, 63.16, 72.18, 0, 0, 0, 0, 6, 68.58, 84.54, 120.51};
	
	
	public String[] getObservationSpace() {
		return observationSpace;
	}


	public String getObservationSpaceName() {
		return "Month";
	}


	public Number[] getCharacteristicValue() {
		return characteristicValues;
	}


	public String getChararacteristicValueName() {
		return "Heat Demand [MWh]";
	}


	public String getTopic() {
		return "Heat Demand HFT Stuttgart Building 2 (per month)";
	}
	
}
